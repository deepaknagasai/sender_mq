package com.consumer.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

//Tell spring that it is Spring Applications use this annotation


//this annotation tells spring boot that this is the starting point of spring boot application
//Strat this application "EmployeeApiApp" and then create a servelt container and host this application
// in a servlet container and make it available.
// this servlet hosted in tomcat no need to configure this..
//in spring we have a server side code which maps to urls and gives the response
@SpringBootApplication
public class MessageSenderApi extends SpringBootServletInitializer  {

     //Used when run as JAR

    public static void main(String[] args) {
        SpringApplication.run(MessageSenderApi.class, args);
    }

    // Used when run as WAR
     
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(MessageSenderApi.class);
    }

}