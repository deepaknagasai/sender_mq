package com.consumer.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.jms.QueueSender;

	
@RestController
public class Controller {
	String xmlobject = "<Employee>\n" + "  <FirstName>Ajay</FirstName>\n"
			+ "  <LastName>Kumar</LastName>\n" + "  <Id>512</Id>\n" + "</Employee>";
		
		// Bydefault all the list values conveted in JSON format because
		// class declared as a rest controller
		@RequestMapping("/send")
		public String receiveMessages() {
			try {
				QueueSender.connect(xmlobject);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "Messages  posted to MQ....!";
		}

}
