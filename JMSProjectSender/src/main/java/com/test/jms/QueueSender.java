package com.test.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueueSender {


	public static void connect(String xmlobject)throws Exception {
		
		// Get the connection Factory
				//ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
				//ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://12.28.130.100:61616");
				ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://devopsapp.southcentralus.cloudapp.azure.com:61616");
				// Get The connection
				Connection connection = connectionFactory.createConnection();

				// Start the connection
				connection.start();

				Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

				// Queue
				Destination queue = session.createQueue("requestQ");

				// QueueSender
				MessageProducer producer = session.createProducer(queue);

				// Create Message
				//for (int i = 0; i < 10; i++) {
					//TextMessage message = session.createTextMessage("Test Message " + i);
					// sending message
				//	producer.send(message);
					//System.out.println(message.getJMSMessageID());
				//}
				
				TextMessage message = session.createTextMessage(xmlobject);
				for(int i =1; i<=2;i++){
				producer.send(message);
				// closing connection
				}
				connection.close();



	}


}
